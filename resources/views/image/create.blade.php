<form method="post" action="{{route('images.update', '1')}}" enctype="multipart/form-data">
  {{ csrf_field() }}
  {{ method_field('PUT')}}

  <label>Sobe</label>
  <input type="file" name="file" />
  <input type="hidden" name="user_id" value="1" />

  <input type="submit" value="Enviar" />
</form>

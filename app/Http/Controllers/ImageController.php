<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Http\Requests\ImageValidation;
use App\Http\Resources\ImageResource;
use Storage;

class ImageController extends Controller
{

  public function __construct(Image $image ) {
    $this->image = $image;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  public function create () {
    return view('image.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if($request->hasFile('file') ) {

      $request->file->store('public');

      $this->image->path = $this->generateFullPath($request->file->hashName());
      $this->image->name = $request->file->hashName();
      $this->image->size = $request->file->getClientSize();
      $this->image->user_id = $request->user_id;

      $this->image->save();

      return new ImageResource($this->image);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $image = $this->image->where('user_id', $id)->firstOrFail();

    return new ImageResource($image);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if($request->hasFile('file') ) {

      $image = $this->image->where('user_id', $id)->first();

      $request->user_id = $id;

      if(empty($image)) {
        $this->store($request);
      } else {
        if(Storage::disk('public')->exists($image->name)) {
          Storage::disk('public')->delete($image->name);
        }
      }

      $request->file->store('public');
      $image = $this->image->where('user_id', $id)->first();
      $image->path = $this->generateFullPath($request->file->hashName());
      $image->name = $request->file->getClientOriginalName();
      $image->size = $request->file->getClientSize();

      $image->save();

      return new ImageResource($image);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }

  // Helper Functions

  protected function generateFullPath($image_name) {

    $base_path = url()->asset('storage').'/';

    return $base_path.$image_name;
  }
}
